# prose-clock

## What Time Is It?

We've seen digital clocks. Boring. We've seen analog time pieces. Quaintly
geometric. In the world of text-based communication, we expect a timely and
prosaic declaration.

## An Alternative to Digital or Analog

prose-clock displays the time in the manner of a spoken English colloquial
expression. It's aware of the notions of morning, afternoon, and night, thus
keeping AM vs. PM unambiguous.

## Installation

npm i prose-clock

## Usage

### 1. Import the Module

```javascript
import { ProseClock } from 'prose-clock';
```

### 2. Instantiate with or without language style option

Default is conventional, idiomatic English. Set the style in the consructor to `'curse'` or `'regular'` (`regular` is default).

If `'curse'` is used, the time is also expressed in English, but the text is
occasionally laced with colorful slang.

```javascript
// Two ways to instantiate
const pc = new ProseClock();
const pc = new ProseClock('curse');
```

#### 3. Get the time, expressed in text

```javascript
pc.getTime(); // --> "exactly twenty to eight at night"
```

Default is current time. However, you can optionally pass in a Date object:

```javascript
const d = new Date();
d.setHours(12, 45);
pc.getTime(d); // --> "exactly quarter to one in the afternoon"
```
## Notes

In this module's world, granularity is a minute. It would undermine the
philosophy of ProseClock to display, for example, "just after five past nine in
the morning and twenty-four seconds."

## Suggestion

Enhance your app; wrap `pc.getTime()` in a `setInterval()` call, configured to
run every 20 seconds.

Have fun!

Gerry Gold

August 2023
