// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// The MIT License (MIT)
//
// Copyright (c) 2017-Eternity Gerry Gold
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import PROSE_CLOCK from './data/prose-clock.json';
import CURSE_CLOCK from './data/curse-clock.json';

type LanguageStyle = 'regular' | 'curse';

const TWO_OR_MORE_SPACES_RX = / {2,}/;

class ProseClock {
  minutePhrases: string[] = [];
  hourPhrases: string[] = [];
  multiples: Record<string, string> = {};

  mul: number = 0;
  hours: number = 0;
  minutes: number = 0;

  hrPhrase = '';
  minPhrase = '';
  mulPhrase = '';

  constructor(style: LanguageStyle = 'regular') {
    this.setPhrases(style);
  }

  /**
   * getTime() is typically called without an argument, returning a string
   * representing the current time.
   *
   * Call with a date object will return a string representing that date, e.g.:
   *
   * <pre>
   *   const d = new Date();
   *   d.setHours(12, 45);
   *   pc.getTime(d); // -> "exactly quarter to one in the afternoon"
   * </pre>
   */
  public getTime(dateObj: Date = new Date()): string {
    this.setTime(dateObj);
    this.setMinute();
    this.setHour();

    return `${this.minPhrase} ${this.mulPhrase} ${this.hrPhrase}`
      .replace(TWO_OR_MORE_SPACES_RX, ' ');
  }

  private setHour(): void {
    let hours = this.hours;

    if (this.mul >= 35) {
      hours === 23 ? hours = 0 : ++hours;
    }

    this.hrPhrase = this.hourPhrases[hours];

    if ((hours === 0) && this.minutes < 3) {
      this.mulPhrase = '';
    }
  }

  private setMinute(): void {
    const remainder = this.minutes % 5;
    this.mul = this.minutes - remainder;

    if (remainder >= 3) {
      this.mul += 5;
    }

    this.minPhrase = this.minutePhrases[remainder];

    if (String(this.mul) in this.multiples) {
      this.mulPhrase = this.multiples[this.mul];
    } else {
      this.mulPhrase = '';
    }
  }

  private setTime(dateObj: Date): void {
    this.hours = dateObj.getHours();
    this.minutes = dateObj.getMinutes();
  }

  private setPhrases(style: string): void {
    if (style === 'regular') {
      this.minutePhrases = PROSE_CLOCK.minutePhrases;
      this.hourPhrases = PROSE_CLOCK.hourPhrases;
      this.multiples = PROSE_CLOCK.multiples;
    } else {
      this.minutePhrases = CURSE_CLOCK.minutePhrases;
      this.hourPhrases = CURSE_CLOCK.hourPhrases;
      this.multiples = CURSE_CLOCK.multiples;
    }
  }
}

export {
  ProseClock
}
